
import random
import sys

EMPTY = ' '
WALL = '#'
AGENT = 'S'
GOAL = 'E'
VISITED = 'x'


def adjacent(cell):
  i,j = cell
  for (y,x) in ((1,0), (0,1), (-1, 0), (0,-1)):
    yield (i+y, j+x), (i+2*y, j+2*x)

def generate(width, height, verbose=True):
  # add 2 for border walls.

  width += 2 
  height += 2
  rows, cols = height, width

  maze = {}

  spaceCells = set()
  connected = set()
  walls = set()

  # Initialize with grid.
  for i in range(rows):
    for j in range(cols):
      if (i%2 == 1) and (j%2 == 1):
        maze[(i,j)] = EMPTY
      else:
        maze[(i,j)] = WALL 

  # Fill in border.
  for i in range(rows):
    maze[(i,0)] = WALL
    maze[(i,cols-1)] = WALL
  for j in range(cols):
    maze[(0,j)] = WALL
    maze[(rows-1,j)] = WALL

  for i in range(rows):
    for j in range(cols):
      if maze[(i,j)] == EMPTY:
        spaceCells.add((i,j))
      if maze[(i,j)] == WALL:
        walls.add((i,j))

  # Prim's algorithm to knock down walls.
  originalSize = len(spaceCells)
  connected.add((1,1))
  while len(connected) < len(spaceCells):
    doA, doB = None, None
    cns = list(connected)
    random.shuffle(cns)
    for (i,j) in cns:
      if doA is not None: break
      for A, B in adjacent((i,j)):
        if A not in walls: 
          continue
        if (B not in spaceCells) or (B in connected):
          continue
        doA, doB = A, B
        break
    A, B = doA, doB
    maze[A] = EMPTY
    walls.remove(A)
    spaceCells.add(A)
    connected.add(A)
    connected.add(B)

  # Insert character and goals.
  TL = (1,1)
  BR = (rows-2, cols-2)
  if rows % 2 == 0:
    BR = (BR[0]-1, BR[1])
  if cols % 2 == 0:
    BR = (BR[0], BR[1]-1)
    
  global GOALCORDS
  global STARTCORDS
  GOALCORDS = BR
  STARTCORDS = TL
  maze[TL] = AGENT
  maze[BR] = GOAL


  return maze

def search(maze, x, y):

    if maze[(x,y)] == GOAL:
        #print ('found at %d,%d' % (x, y))
        return True
    elif maze[(x,y)] == WALL:
        #print ('wall at %d,%d' % (x, y))
        return False
    elif maze[(x,y)] == VISITED:
        #print ('visited at %d,%d' % (x, y))
        return False
        
    #print ('visiting %d,%d' % (x, y))

    # mark as visited

    maze[(x,y)] = VISITED

    # explore neighbors clockwise starting by the one on the right
    if ((x < len(maze)-1 and search(maze, x+1, y))
        or (y > 0 and search(maze, x, y-1))
        or (x > 0 and search(maze, x-1, y))
        or (y < len(maze)-1 and search(maze, x, y+1))):
        maze[(x,y)] = '.'
        return maze
    return False 



def maze_to_array(maze, height, width):

  width += 2 
  height += 2
  rows, cols = height, width

  lines = []
  for i in range(rows):
     lines.append(''.join(maze[(i,j)] for j in range(cols)))

  return lines

def print_maze(maze):
    print('\n'.join(maze))

def clean_maze(maze, width, height):
    for i in range(width):
        for j in range(height):
            if maze[(i,j)] == VISITED:
                maze[(i,j)] = EMPTY
                
    
    return maze


if __name__ == '__main__':

    width = int(input("Desired width: "))
    height = int(input("Desired height: "))
    
    maze = generate(width, height)
  
    a , b = STARTCORDS

    cleanmaze= clean_maze(search(maze, a, b), width, height)

    printmaze = maze_to_array(cleanmaze, width, height)

    #cleanmaze= clean_maze(printmaze, width, height)

    print_maze(printmaze)
    
